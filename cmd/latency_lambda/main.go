package main

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/eawsy/aws-lambda-go-core/service/lambda/runtime"
	"github.com/eawsy/aws-lambda-go-event/service/lambda/runtime/event/apigatewayproxyevt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/prometheus/blackbox_exporter/config"
	"github.com/prometheus/blackbox_exporter/prober"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
)

const (
	configFile = "blackbox.yml"
)

var (
	logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))

	probers = map[string]prober.ProbeFn{
		"http": prober.ProbeHTTP,
		"tcp":  prober.ProbeTCP,
		"icmp": prober.ProbeICMP,
		"dns":  prober.ProbeDNS,
	}

	errInvalidMethod = errors.New("Invalid HTTP method")
	errMissingTarget = errors.New("Target parameter missing")
	errInvalidModule = errors.New("Invalid module")
	errInvalidProber = errors.New("Invalid prober")

	errInvalidMethodResponse = &Response{StatusCode: http.StatusBadRequest, Body: errInvalidMethod.Error()}
	errMissingTargetResponse = &Response{StatusCode: http.StatusBadRequest, Body: errMissingTarget.Error()}
	errInvalidModuleResponse = &Response{StatusCode: http.StatusBadRequest, Body: errInvalidModule.Error()}
	errInvalidProberResponse = &Response{StatusCode: http.StatusBadRequest, Body: errInvalidProber.Error()}

	sc = &config.SafeConfig{}
)

type Response struct {
	StatusCode      int               `json:"statusCode"`
	Headers         map[string]string `json:"headers,omitempty"`
	Body            string            `json:"body,omitempty"`
	IsBase64Encoded bool              `json:"isBase64Encoded"`
}

func init() {
	if err := sc.ReloadConfig(configFile); err != nil {
		level.Error(logger).Log("msg", "Error loading config", err)
		os.Exit(1)
	}
}

func main() {}

func Handle(event *apigatewayproxyevt.Event, rctx *runtime.Context) (*Response, error) {
	/*
		if event.HTTPMethod != "GET" {
			return nil, errInvalidMethod
		}*/
	level.Debug(logger).Log("event", event)
	/*
		level.Debug(logger).Log("event", event)
		level.Debug(logger).Log("event.QueryStringParameters", event.QueryStringParameters)
	*/
	target := event.QueryStringParameters["target"]
	if target == "" {
		return errMissingTargetResponse, nil
	}
	moduleName := event.QueryStringParameters["moduleName"]
	if moduleName == "" {
		moduleName = "http_2xx"
	}
	module, ok := sc.C.Modules[moduleName]
	if !ok {
		return errInvalidModuleResponse, nil
	}
	prober, ok := probers[module.Prober]
	if !ok {
		return errInvalidProberResponse, nil
	}
	registry := prometheus.NewRegistry()
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(10*time.Second))
	defer cancel()
	prober(ctx, target, module, registry, logger)
	mfs, err := registry.Gather()
	if err != nil {
		return nil, err
	}
	headers := http.Header{}
	for k, v := range event.Headers {
		headers.Add(k, v)
	}
	contentType := expfmt.Negotiate(http.Header(headers))
	buf := &bytes.Buffer{}

	enc := expfmt.NewEncoder(buf, contentType)
	for _, mf := range mfs {
		if err := enc.Encode(mf); err != nil {
			return nil, err
		}
	}

	return &Response{
		StatusCode: http.StatusOK,
		Headers: map[string]string{
			"Content-Type":   string(contentType),
			"Content-Length": fmt.Sprint(buf.Len()),
		},
		Body: buf.String(),
	}, nil
}
