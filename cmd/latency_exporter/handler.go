package main

import (
	"context"
	"crypto/subtle"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/prometheus/blackbox_exporter/config"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/latency.at/latencyAt/errors"
)

var (
	requestCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "exporter",
			Name:      "requests_total",
			Help:      "Total number of requests made by latency exporter",
		},
	)
	requestDuration = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "lat",
			Subsystem: "exporter",
			Name:      "request_duration_seconds",
			Help:      "Duration of request made by latency exporter histogram",
			Buckets:   prometheus.ExponentialBuckets(0.01, 5, 4),
		},
	)
	errorCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "exporter",
			Name:      "errors_total",
			Help:      "Total number of error while handling requests made by latency exporter",
		},
	)
)

func init() {
	prometheus.MustRegister(requestCounter)
	prometheus.MustRegister(requestDuration)
	prometheus.MustRegister(errorCounter)
}

type Handler struct {
	Booker       *Booker
	ProxyHandler http.HandlerFunc
	Config       *config.SafeConfig
	ReloadCh     chan chan error
	AuthPassword string
	AuthUsername string
	LogLevel     level.Option
}

func ProbeHandler(w http.ResponseWriter, r *http.Request, c *config.Config) {
	moduleName := r.URL.Query().Get("module")
	if moduleName == "" {
		moduleName = "http_2xx"
	}
	module, ok := c.Modules[moduleName]
	if !ok {
		http.Error(w, fmt.Sprintf("Unknown module %q", moduleName), 400)
		return
	}

	// If a timeout is configured via the Prometheus header, add it to the request.
	var timeoutSeconds float64
	if v := r.Header.Get("X-Prometheus-Scrape-Timeout-Seconds"); v != "" {
		var err error
		timeoutSeconds, err = strconv.ParseFloat(v, 64)
		if err != nil {
			http.Error(w, fmt.Sprintf("Failed to parse timeout from Prometheus header: %s", err), http.StatusInternalServerError)
			return
		}
	}
	if timeoutSeconds == 0 {
		timeoutSeconds = 10
	}

	if module.Timeout.Seconds() < timeoutSeconds && module.Timeout.Seconds() > 0 {
		timeoutSeconds = module.Timeout.Seconds()
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration((timeoutSeconds-*timeoutOffset)*1e9))
	defer cancel()
	r = r.WithContext(ctx)

	probeSuccessGauge := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "probe_success",
		Help: "Displays whether or not the probe was a success",
	})
	probeDurationGauge := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "probe_duration_seconds",
		Help: "Returns how long the probe took to complete in seconds",
	})
	params := r.URL.Query()
	target := params.Get("target")
	if target == "" {
		http.Error(w, "Target parameter is missing", 400)
		return
	}

	prober, ok := Probers[module.Prober]
	if !ok {
		http.Error(w, fmt.Sprintf("Unknown prober %q", module.Prober), 400)
		return
	}

	start := time.Now()
	registry := prometheus.NewRegistry()
	registry.MustRegister(probeSuccessGauge)
	registry.MustRegister(probeDurationGauge)

	rlogger := &filterLogger{
		skip:   "Resolution with preferred IP protocol failed, attempting fallback protocol",
		Logger: log.With(logger, "caller", log.Caller(6), "module", moduleName, "target", target),
	}
	success := prober(ctx, target, module, registry, level.NewFilter(rlogger, probeLogLevel))
	probeDurationGauge.Set(time.Since(start).Seconds())
	if success {
		probeSuccessGauge.Set(1)
	}
	promhttp.HandlerFor(registry, promhttp.HandlerOpts{}).ServeHTTP(w, r)
}

func (h *Handler) Authenticate(r *http.Request) error {
	if h.AuthPassword == "" {
		return nil
	}
	username, password, ok := r.BasicAuth()
	if !ok {
		return errors.ErrAuthRequired
	}
	if !(subtle.ConstantTimeCompare([]byte(username), []byte(h.AuthUsername)) == 1 &&
		subtle.ConstantTimeCompare([]byte(password), []byte(h.AuthPassword)) == 1) {
		return errors.ErrCredentialsUserInvalid
	}
	return nil
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch {
	case strings.HasPrefix(r.URL.Path, "/-/reload"):
		if err := h.Authenticate(r); err != nil {
			http.Error(w, err.Error(), http.StatusForbidden)
			return
		}
		if r.Method != "POST" {
			w.WriteHeader(http.StatusMethodNotAllowed)
			fmt.Fprintf(w, "This endpoint requires a POST request.\n")
			return
		}

		rc := make(chan error)
		h.ReloadCh <- rc
		if err := <-rc; err != nil {
			http.Error(w, fmt.Sprintf("failed to reload config: %s", err), http.StatusInternalServerError)
		}
	case strings.HasPrefix(r.URL.Path, "/metrics"):
		if err := h.Authenticate(r); err != nil {
			http.Error(w, err.Error(), http.StatusForbidden)
			return
		}
		if strings.HasPrefix(r.URL.Path, "/metrics/proxy") {
			h.ProxyHandler(w, r)
			return
		}

		promhttp.Handler().ServeHTTP(w, r)

		return
	case strings.HasPrefix(r.URL.Path, "/probe"):
		start := time.Now()
		if !*bookDisabled {
			requestCounter.Inc()
			err, status := h.Booker.Validate(r)
			if err != nil {
				// Abort request if response indicates user error / too low balance...
				if status == http.StatusBadRequest || status == http.StatusForbidden || status == http.StatusPaymentRequired {
					http.Error(w, err.Error(), status)
					return
				}
				// ...but still serve it if it's not, e.g fails because of issues on our side.
				level.Error(logger).Log("msg", "Error booking request", "error", err)

				// FIXME: helpful to know if due to DNS, connect or app level failure?
				// What should be the pattern in general?
				errorCounter.Inc()
			}
		}

		requestDuration.Observe(time.Since(start).Seconds())
		h.Config.Lock()
		conf := h.Config.C
		sc.Unlock()
		ProbeHandler(w, r, conf)
		h.Booker.Log(r)

	case r.URL.Path == "/":
		w.Write([]byte(indexHTML))
	}
}

type filterLogger struct {
	log.Logger
	skip string
}

func (l *filterLogger) Log(keyvals ...interface{}) error {
	for i := 0; i < len(keyvals); i += 2 {
		if keyvals[i] != "msg" {
			continue
		}
		if keyvals[i+1] == l.skip {
			return nil
		}
		break
	}
	return l.Logger.Log(keyvals...)
}
