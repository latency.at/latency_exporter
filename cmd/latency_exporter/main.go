// Copyright 2016 The Prometheus Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"crypto/tls"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	"golang.org/x/crypto/acme"
	"golang.org/x/crypto/acme/autocert"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/version"
	kingpin "gopkg.in/alecthomas/kingpin.v2"

	"github.com/prometheus/blackbox_exporter/config"
	"github.com/prometheus/blackbox_exporter/prober"
)

var (
	sc = &config.SafeConfig{
		C: &config.Config{},
	}

	logger        = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	probeLogLevel = level.AllowWarn()

	configFile  = kingpin.Flag("config.file", "Blackbox exporter configuration file.").Default("blackbox.yml").String()
	listenHTTP  = kingpin.Flag("web.listen-address", "The address to listen on for HTTP requests.").Default(":9115").String()
	listenHTTPS = kingpin.Flag("web.listen-address-https", "If given, listen for HTTPS on this address.").Default("").String()

	acmeDomain    = kingpin.Flag("acme.domain", "Domain to use for ACME.").Default("example.com").String()
	acmeEmail     = kingpin.Flag("acme.email", "Email to use for ACME.").Default("hostmaster@example.com").String()
	acmeDirectory = kingpin.Flag("acme.url", "URL for acme directory.").Default(acme.LetsEncryptURL).String()
	acmeCache     = kingpin.Flag("acme.cache", "Directory for acme cache.").Default("/var/cache/acme").String()

	authPassword = kingpin.Flag("auth.pass", "If set, require Basic Auth for /metrics and /-/reload endpoints.").Default("").String()
	authUsername = kingpin.Flag("auth.user", "Username to require when Basic Auth is enabled.").Default("admin").String()

	bookURL      = kingpin.Flag("book.url", "Booking URL").Default("http://admin:foobar23@localhost:8000/api/book").String()
	bookDisabled = kingpin.Flag("book.disabled", "Disable Booking").Default("false").Bool()
	proxyURL     = kingpin.Flag("proxy.url", "Proxy URL").Default("http://localhost:9100/metrics").String()

	timeoutOffset = kingpin.Flag("timeout-offset", "Offset to subtract from timeout in seconds.").Default("0.5").Float64()
	debug         = kingpin.Flag("debug", "Enable debug logging.").Default("false").Bool()

	psProjectID = kingpin.Flag("pubsub.project", "Google pubsub project ID").Default("latency-at").String()
	psTopicName = kingpin.Flag("pubsub.topic", "Google pubsub topic name").Default("latency-exporter-requests").String()

	Probers = map[string]prober.ProbeFn{
		"http": prober.ProbeHTTP,
		"tcp":  prober.ProbeTCP,
		"icmp": prober.ProbeICMP,
		"dns":  prober.ProbeDNS,
	}
)

func init() {
	prometheus.MustRegister(version.NewCollector("blackbox_exporter"))
}

func main() {
	// log.AddFlags(kingpin.CommandLine)
	kingpin.Version(version.Print("blackbox_exporter"))
	kingpin.HelpFlag.Short('h')
	kingpin.Parse()
	if *debug {
		probeLogLevel = level.AllowAll()
	} else {
		logger = level.NewFilter(logger, level.AllowInfo())
	}
	mlogger := log.With(logger, "caller", log.DefaultCaller)
	level.Info(mlogger).Log("msg", "Starting blackbox_exporter", version.Info())
	level.Info(mlogger).Log("msg", "Build context", version.BuildContext())

	if err := sc.ReloadConfig(*configFile); err != nil {
		level.Error(mlogger).Log("msg", "Error loading config", err)
		os.Exit(1)
	}
	level.Info(mlogger).Log("Loaded config file")

	hup := make(chan os.Signal)
	reloadCh := make(chan chan error)
	signal.Notify(hup, syscall.SIGHUP)
	go func() {
		for {
			select {
			case <-hup:
				if err := sc.ReloadConfig(*configFile); err != nil {
					level.Error(mlogger).Log("msg", "Error reloading config", "file", *configFile, "error", err)
					continue
				}
				level.Info(logger).Log("Loaded config file")
			case rc := <-reloadCh:
				if err := sc.ReloadConfig(*configFile); err != nil {
					level.Error(mlogger).Log("msg", "Error reloading config", "file", *configFile, "error", err)
					rc <- err
				} else {
					level.Info(mlogger).Log("msg", "Loaded config file")
					rc <- nil
				}
			}
		}
	}()

	burl, err := url.Parse(*bookURL)
	if err != nil {
		level.Error(mlogger).Log("msg", "Book URL invalid", "url", *bookURL, "error", err)
		os.Exit(1)
	}
	booker, err := newBooker(burl.String(), *psProjectID, *psTopicName)
	if err != nil {
		level.Error(mlogger).Log("msg", "Couldn't create booker", "error", err)
		os.Exit(1)
	}
	server := &http.Server{
		Handler: &Handler{
			Booker: booker,
			ProxyHandler: func(w http.ResponseWriter, r *http.Request) {
				resp, err := http.Get(*proxyURL)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}
				defer resp.Body.Close()
				_, err = io.Copy(w, resp.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}
			},
			Config:       sc,
			ReloadCh:     reloadCh,
			AuthPassword: *authPassword,
			AuthUsername: *authUsername,
		},
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	if *listenHTTPS != "" {
		acm := &autocert.Manager{
			Client: &acme.Client{
				DirectoryURL: *acmeDirectory,
			},
			Cache:      autocert.DirCache(*acmeCache),
			Email:      *acmeEmail,
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(*acmeDomain),
		}
		server.Handler = acm.HTTPHandler(server.Handler)

		server.TLSConfig = &tls.Config{
			GetCertificate: acm.GetCertificate,
		}
		go func() {
			ln, err := tls.Listen("tcp", *listenHTTPS, server.TLSConfig)
			if err != nil {
				level.Error(mlogger).Log("msg", "Error listening", "addr", *listenHTTPS, "error", err)
				os.Exit(1)
			}
			if err := server.Serve(ln); err != nil {
				level.Error(mlogger).Log("msg", err)
				os.Exit(1)
			}
		}()
	}

	if *listenHTTP != "" {
		go serve(mlogger, *listenHTTP, server)
	}

	shutdown := make(chan os.Signal)
	signal.Notify(shutdown, syscall.SIGTERM)
	signal.Notify(shutdown, syscall.SIGINT)
	<-shutdown
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	level.Info(mlogger).Log("msg", "Shutting down")
	if err := server.Shutdown(ctx); err != nil {
		level.Error(mlogger).Log("msg", "Error shutting down server", "error", err)
		os.Exit(1)
	}
	level.Info(mlogger).Log("Successfully down")
}

func serve(log log.Logger, addr string, server *http.Server) {
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		level.Error(logger).Log("msg", "Error listening", "addr", addr, "error", err)
		os.Exit(1)
	}
	level.Info(logger).Log("msg", "Listening", "addr", addr)
	if err := server.Serve(ln); err != http.ErrServerClosed {
		level.Error(logger).Log("msg", "Error serving", "addr", addr, "error", err)
		os.Exit(1)
	}
}
