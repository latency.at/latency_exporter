# latency\_exporter

The Latency Exporter runs on [Latency.At](https://latency.at)'s Probes to
provide performance and availability metrics.

It's built around https://github.com/prometheus/blackbox_exporter to
add acme/let's encrypt, accounting and internal metrics.
